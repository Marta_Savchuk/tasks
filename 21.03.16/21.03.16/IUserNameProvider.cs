﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21._03._16
{
    interface IUserNameProvider
    {
       string GetUserName();
        
    }
}
