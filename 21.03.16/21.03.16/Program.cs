﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21._03._16
{
    public class Program
    {

       /* public static void writeName(string name) {
            Console.WriteLine("Hello, " + name);
        }*/
        static void printConsole(IUserNameProvider name)
        {
            Console.WriteLine("Hi " + name.GetUserName());
        }
        static void printEasy(IUserNameProvider name)
        {
            Console.WriteLine(name.GetUserName());
        }

        public static void Main(string[] args)
        {
            //writeName("Marta");
            //Console.Read();

            IUserNameProvider name;

            printConsole (name = new EasyLevelUserNameProvider());
            printEasy (name = new ConsoleUserNameProvider());

            Console.Read();
        }
    }
}
