﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21._03._16
{
    class ConsoleUserNameProvider:IUserNameProvider
    {
       string IUserNameProvider.GetUserName() {
            string name;
            name = Console.ReadLine();
            return name;
        }
    }
}
